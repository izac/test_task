<?php

namespace Interfaces;

interface IValidator
{
    public function validate(): array;

    public function setFiels(array $fields): void;

    public function setRules(array $rules): void;

    public function setValidateObject(object $object): void;
}
