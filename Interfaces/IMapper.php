<?php

namespace Interfaces;

interface IMapper
{
    public function setJson(array $json): void;

    public function mappedJsonToObject(): object;
}
