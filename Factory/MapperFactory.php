<?php

namespace Factory;

use Interfaces\IMapper;
use Psr\Http\Message\RequestInterface;

abstract class MapperFactory
{
    abstract public function getMapper(): IMapper;

    public function mapped(RequestInterface $request): object
    {
        $mapper = $this->getMapper();
        $json = json_decode($request->getBody(), true);
        $mapper->setJson($json);
        $object = $mapper->mappedJsonToObject();

        return $object;
    }
}
