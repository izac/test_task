<?php

namespace Helpers;

use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\RequestInterface;

class RequestHelper
{
    public static function createRequest(string $data): RequestInterface
    {
        $json = json_decode($data, true);
        $randKey = array_rand($json);
        $data = json_encode($json[$randKey]);
        $request = new Request("POST", 'http://localhost', [], $data);

        return $request;
    }
}
