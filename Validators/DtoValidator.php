<?php
namespace Validators;

use Helpers\Validation;

class DtoValidator extends BaseValidator
{
    private $dtoObject;
    private $validatationLib;

    public $rules = [
        "firstName" => "words",
        "lastName" => "words",
        "emailAddress" => "email",
        "phoneNumber" => ["regexp" => "\+380[0-9]{9}"],
    ];

    public function __construct()
    {
        $this->validatationLib = new Validation();
    }

    public function validate(): array
    {
        foreach($this->rules as $field => $rule) {
            if (array_key_exists($field, $this->fields)) {
                if(!is_array($rule)) {
                    $this->validatationLib->name($field)->value($this->fields[$field])->pattern($rule)->required();
                } else {
                    if(!empty($rule["regexp"])) {
                        $this->validatationLib->name($field)->value($this->fields[$field])->customPattern($rule["regexp"])->required();
                    }
                }
            }
        }

        return !empty($this->validatationLib->errors) ? $this->validatationLib->errors : [];
    }

    private function getFieldsFromDto(): array
    {
        return [
            "firstName" => $this->dtoObject->getFirstName(),
            "lastName" => $this->dtoObject->getLastName(),
            "phoneNumber" => $this->dtoObject->getPhoneNumber(),
            "emailAddress" => $this->dtoObject->getEmailAddress(),
        ];
    }

    public function setValidateObject(object $dto): void
    {
        $this->dtoObject = $dto;
        $this->setFiels($this->getFieldsFromDto());
    }
}