<?php

namespace Validators;

use Interfaces\IValidator;

abstract class BaseValidator implements IValidator
{
    protected $fields = [];
    protected $rules = [];

    public function setFiels(array $fiels): void
    {
        $this->fields = $fiels;
    }

    abstract public function validate(): array;

    public function setRules(array $rules): void
    {
        $this->rules = $rules;
    }
}
