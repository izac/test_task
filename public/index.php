<?php

require_once '../vendor/autoload.php';

use Controllers\MainController;
use Mappers\JsonMapperCreater;
use Validators\DtoValidator;

$mapperCreater = new JsonMapperCreater(); // это должно быть в DI для возможности подмены реализации
$validator = new DtoValidator();

$mainCntrl =  new MainController($mapperCreater, $validator);

$jsonRequest = file_get_contents("php://input");
if (!empty($jsonRequest)) {
    $mainCntrl->validateRequst($jsonRequest);
}
