<?php

namespace Controllers;

class BaseController
{

    public function renderRespounce(array $data = []): void
    {
        if (empty($data)) {
            http_response_code(200);
        } else {
            http_response_code(400);
        }

        echo json_encode($data);
    }
}
