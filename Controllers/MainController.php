<?php

namespace Controllers;

use Helpers\RequestHelper;
use Factory\MapperFactory;
use Interfaces\IValidator;

class MainController extends BaseController
{
    private $mapperFactory;
    private $validator;

    public function __construct(MapperFactory $mapperFactory, IValidator $validator)
    {
        $this->mapperFactory = $mapperFactory;
        $this->validator = $validator;
    }

    public function validateRequst(string $data)
    {
        $requestObj = RequestHelper::createRequest($data);
        $object = $this->mapperFactory->mapped($requestObj);
        $this->validator->setValidateObject($object);
        $result = $this->validator->validate();
        $this->renderRespounce($result);
    }
}
