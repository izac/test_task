<?php

namespace Mappers;

use Closure;
use Interfaces\IMapper;
use UserDto;

class JsonMapper implements IMapper
{
    private $dto;
    private $json;

    public function __construct()
    {
        $this->dto = new UserDto();
    }

    public function setJson(array $json): void
    {
        $this->json = $json;
    }

    public function mappedJsonToObject(): object
    {
        $closuer = Closure::bind(function ($json) {
            foreach ($json as $key => $val) {
                if ($key === "email") {
                    $key = "emailAddress";
                }
                if (property_exists($this, $key)) {
                    $this->{$key} = $val;
                }
            }

            return $this;
        }, $this->dto, UserDto::class);
        $dto = $closuer($this->json);

        return $dto;
    }
}
