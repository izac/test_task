<?php

namespace Mappers;

use Factory\MapperFactory;
use Interfaces\IMapper;

class JsonMapperCreater extends MapperFactory
{
    public function getMapper(): IMapper
    {
        return new JsonMapper();
    }
}
